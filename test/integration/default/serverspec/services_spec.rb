require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe service('php5-fpm') do
	it { should be_enabled }
	it { should be_running }
end

describe service('mysql') do
	it { should be_enabled }
	it { should be_running }
end

describe service('nginx') do
	it { should be_enabled }
	it { should be_running }
end