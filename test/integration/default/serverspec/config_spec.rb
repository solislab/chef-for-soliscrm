require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe file('/home/vagrant/.gitconfig') do
	it { should be_file }
end

describe file('/home/vagrant/.profile') do
	it { should be_file }
	it { should contain 'source /home/vagrant/.solis-alias' }
end

describe file('/home/vagrant/.solis-alias') do
	it { should be_file }
	it { should be_owned_by 'vagrant' }
	it { should be_executable.by 'owner' }
end