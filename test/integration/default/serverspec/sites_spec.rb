require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe file('/vagrant/www') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
	it { should be_readable.by 'group' }
	it { should be_executable.by 'group' }
end

describe file('/vagrant/www/wp-content') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
	it { should be_readable.by 'group' }
	it { should be_executable.by 'group' }
end

describe file('/vagrant/www/dev') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
	it { should be_readable.by 'group' }
	it { should be_executable.by 'group' }
end

describe file('/vagrant/www/dev/wp-config.php') do
	it { should be_file }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
	it { should be_readable.by 'group' }

	it { should contain "define('DB_NAME', 'soliscrm_dev');" }
	it { should contain "define('DB_USER', 'dev_user');" }
	it { should contain "define('DB_PASSWORD', 'soliscrm dev user');" }
	it { should contain "define('DB_HOST', 'localhost');" }
	it { should contain "define( 'WP_CONTENT_DIR', '/vagrant/www/wp-content' );" }
	it { should contain "define( 'WP_CONTENT_URL', 'http://localhost:1988/wp-content/' );" }
end

describe file('/vagrant/www/behat/wp-config.php') do
	it { should be_file }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
	it { should be_readable.by 'group' }

	it { should contain "define('DB_NAME', 'soliscrm_behat');" }
	it { should contain "define('DB_USER', 'behat_user');" }
	it { should contain "define('DB_PASSWORD', 'soliscrm behat user');" }
	it { should contain "define('DB_HOST', 'localhost');" }
	it { should contain "define( 'WP_CONTENT_DIR', '/vagrant/www/wp-content' );" }
	it { should contain "define( 'WP_CONTENT_URL', 'http://localhost:1988/wp-content/' );" }
end

describe command('curl -I http://localhost:1988/dev/') do
	its(:stdout) { should match /HTTP\/1.1 200 OK/ }
	its(:stdout) { should match /X-Pingback: http:\/\/localhost:1988\/dev\/xmlrpc\.php/ }
end

describe command('curl -I http://localhost:1988/behat/') do
	its(:stdout) { should match /HTTP\/1.1 200 OK/ }
	its(:stdout) { should match /X-Pingback: http:\/\/localhost:1988\/behat\/xmlrpc\.php/ }
end

describe file('/vagrant/www/dev/install.php') do
	it { should_not be_file }
end

describe file('/vagrant/www/behat/install.php') do
	it { should_not be_file }
end

describe file('/vagrant/www/wp-content/plugins/soliscrm') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
	it { should be_readable.by 'group' }
	it { should be_executable.by 'group' }
end