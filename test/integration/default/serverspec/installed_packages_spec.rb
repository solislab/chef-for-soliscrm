require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe package('php5-fpm') do
	it { should be_installed }
end

describe package('nginx') do
	it { should be_installed }
end

describe package('mysql-server') do
	it { should be_installed }
end

describe package('php5-cli') do
	it { should be_installed }
end

describe package('php5-json') do
	it { should be_installed }
end

describe package('php5-curl') do
	it { should be_installed }
end

describe package('php-pear') do
	it { should be_installed }
end