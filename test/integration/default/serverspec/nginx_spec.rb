require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe file('/etc/nginx/sites-enabled/localhost') do
	it { should be_file }
	it { should be_owned_by 'root' }
	it { should be_readable.by_user 'root' }
	it { should contain 'listen 1988;' }
	it { should contain 'root /vagrant/www;' }
	it { should contain 'fastcgi_pass unix:/var/run/php-fpm-localhost.sock;' }
	it { should contain 'try_files $uri $uri/ /index.php?q=$uri&$args;' }
end

describe port(1988) do
	it { should be_listening }
end

describe user('www-data') do
	it { should exist }
	it { should belong_to_group 'vagrant' }
end

describe file('/etc/php5/fpm/pool.d/localhost.conf') do
	it { should be_file }
	it { should be_owned_by 'root' }
	it { should be_readable.by_user 'root' }
	it { should contain 'user = vagrant' }
	it { should contain 'group = vagrant' }
	it { should contain 'listen = /var/run/php-fpm-localhost.sock' }
	it { should contain 'php_flag[opcache.enable] = 1' }
	it { should contain 'php_value[opcache.memory_consumption] = 64' }
end