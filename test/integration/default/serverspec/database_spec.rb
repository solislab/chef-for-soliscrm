require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe command('mysql -u root -pdefault -e "show databases"') do
	its(:stdout) { should match /soliscrm_behat/ }
	its(:stdout) { should match /soliscrm_dev/ }
	its(:stdout) { should match /soliscrm_unit/ }
end

describe command('mysql -u root -pdefault -e "show grants for unit_user@localhost"') do
	its(:stdout) { should match /GRANT ALL PRIVILEGES ON `soliscrm_unit`.* TO 'unit_user'@'localhost'/ }
end

describe command('mysql -u root -pdefault -e "show grants for dev_user@localhost"') do
	its(:stdout) { should match /GRANT ALL PRIVILEGES ON `soliscrm_dev`.* TO 'dev_user'@'localhost'/ }
end

describe command('mysql -u root -pdefault -e "show grants for behat_user@localhost"') do
	its(:stdout) { should match /GRANT ALL PRIVILEGES ON `soliscrm_behat`.* TO 'behat_user'@'localhost'/ }
end