require 'serverspec'

include Serverspec::Helper::Exec
include Serverspec::Helper::DetectOS

describe file('/vagrant/www/tests/behat-custom.yml') do
	it { should contain 'admin_username: admin' }
	it { should contain 'admin_password: admin' }
	it { should contain 'base_url: http://localhost:1988/behat' }
end

describe command('phpunit -h') do
	it { should return_exit_status 0 }
end

describe file('/vagrant/www/tests') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_grouped_into 'vagrant' }
end

describe file('/vagrant/www/tests/bin/behat') do
	it { should be_file }
	it { should be_owned_by 'vagrant' }
	it { should be_readable.by 'owner' }
	it { should be_executable.by 'owner' }
end

describe file('/vagrant/www/tests/vendor') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_readable.by 'owner' }
	it { should be_executable.by 'owner' }
end

describe file('/vagrant/www/tests/vendor/wordpress-unit-test') do
	it { should be_directory }
	it { should be_owned_by 'vagrant' }
	it { should be_readable.by 'owner' }
	it { should be_executable.by 'owner' }
end

describe file('/vagrant/www/tests/vendor/wordpress-unit-test/wp-tests-config.php') do
	it { should be_file }
	it { should be_owned_by 'vagrant' }
	it { should be_readable.by 'owner' }
	it { should contain "define( 'DB_NAME', 'soliscrm_unit' );"}
	it { should contain "define( 'DB_USER', 'unit_user' );"}
	it { should contain "define( 'DB_PASSWORD', 'soliscrm unit user' );"}
end

describe file('/vagrant/www/tests/phpunit-config.php') do
	it { should be_file }
	it { should be_owned_by 'vagrant' }
	it { should be_readable.by 'owner' }
	it { should contain "define( 'SOLISCRM_TEST_PLUGIN_PATH', '/vagrant/www/wp-content/plugins/soliscrm/soliscrm.php' );" }
end