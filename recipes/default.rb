::Chef::Recipe.send(:include, Opscode::OpenSSL::Password)

# Install LEMP stack
include_recipe 'php-fpm'
include_recipe 'nginx'
include_recipe 'database::mysql'
include_recipe 'mysql::server'
include_recipe 'git'

package 'curl'
package 'php5-cli'
package 'php5-json'
package 'php5-curl'
package 'php-pear'

package 'php5-mysql' do
	case node['platform_family']
	when 'rhel', 'fedora', 'suse'
		package_name 'php-mysqlnd'
	when 'arch'
		package_name 'php-mysql'
	when 'gentoo'
		package_name 'dev-lang/php/mysql'
	end
end

mysql_connection_info = {
	:host     => 'localhost',
	:username => 'root',
	:password => node['mysql']['server_root_password']
}

www_path = '/vagrant/www'
wp_content_path = '/vagrant/www/wp-content'
tests_path = '/vagrant/www/tests'

# Add www-data to "vagrant" group so that nginx can access the development sites
group "vagrant" do
	action  :modify
	members "www-data"
	append  true
end

# Nginx configuration
template "#{node['nginx']['dir']}/sites-available/localhost" do
	source 'default-site.erb'
	owner 'root'
	group 'root'
	mode  '0644'
	variables(
		:name       => "localhost",
		:port       => "1988",
		:php_listen => "unix:/var/run/php-fpm-localhost.sock",
		:rewrite    => true,
		:hostname   => "localhost",
		:root       => www_path
	)
	notifies :reload , 'service[nginx]', :delayed
	notifies :restart, 'service[php-fpm]', :delayed
end

# PHP Pool for this site
php_fpm_pool "localhost" do
	listen "/var/run/php-fpm-localhost.sock"
	user "vagrant"
	group "vagrant"
	php_options({
		'php_flag[opcache.enable]' => 1,
		'php_value[opcache.memory_consumption]' => 64
	})
end

# Enable nginx site
nginx_site "localhost"

# Download and extract latest WordPress to a cache folder
wordpress_tar = "#{Chef::Config[:file_cache_path]}/wordpress.tar.gz"
wordpress_src = "#{Chef::Config[:file_cache_path]}/"
remote_file wordpress_tar do
	source "http://wordpress.org/latest.tar.gz"
	group  "vagrant"
	owner  "vagrant"
	checksum "2c5c56dc77cc891eca3782a04e2706dcd460a7ebd96de03221bdc414b7eb3992"
	mode   0644
end

# Create vagrant directory in case it does not yet exist
directory "/vagrant" do
	owner "vagrant"
	group "vagrant"
	mode "0750"
end

directory www_path do
	owner "vagrant"
	group "vagrant"
	mode "0750"
end

directory wp_content_path do
	owner "vagrant"
	group "vagrant"
	mode "0750"
end

bash 'extract_wordpress_dev' do
	cwd  ::File.dirname(wordpress_tar)
	user 'root'
	code <<-EOH
		tar --keep-newer-files -xzf #{wordpress_tar} -C #{wordpress_src}
		cp -Ru #{wordpress_src}/wordpress/wp-content/* #{wp_content_path}/
	EOH
end

mysql_database "soliscrm_unit" do
	connection mysql_connection_info
end

mysql_database_user "unit_user" do
	connection mysql_connection_info
	password   "soliscrm unit user"
	database_name "soliscrm_unit"
	privileges [:all]
	action :grant
end

#
# DEV SITE
#
mysql_database "soliscrm_dev" do
	connection mysql_connection_info
end

mysql_database_user "dev_user" do
	connection mysql_connection_info
	password   "soliscrm dev user"
	database_name "soliscrm_dev"
	privileges [:all]
	action :grant
end

site_path = "#{www_path}/dev"
directory site_path do
	owner "vagrant"
	group "vagrant"
	mode "0750"
end

bash 'copy_wordpress_dev' do
	user "root"
	code "cp -Ru #{wordpress_src}/wordpress/* #{site_path}/"
end

# Create config file
vars = {
	:auth_key => secure_password(128),
	:secure_auth_key => secure_password(128),
	:logged_in_key => secure_password(128),
	:nonce_key => secure_password(128),
	:auth_salt => secure_password(128),
	:secure_auth_salt => secure_password(128),
	:logged_in_salt => secure_password(128),
	:nonce_salt => secure_password(128),
	:db_name => 'soliscrm_dev',
	:db_user => 'dev_user',
	:db_pass => 'soliscrm dev user',
	:db_host => 'localhost',
	:prefix => 'wp_',
	:wp_content_dir => wp_content_path,
	:wp_content_url => 'http://localhost:1988/wp-content/'
}
template "#{site_path}/wp-config.php" do
	source 'wp-config.php.erb'
	owner 'vagrant'
	group 'vagrant'
	variables vars
	mode  '0644'
	not_if { File.exists? "#{site_path}/wp-config.php" }
end

template "#{site_path}/install.php" do
	source 'install.php.erb'
	owner 'vagrant'
	group 'vagrant'
	variables(
		:home_url => 'http://localhost:1988/dev/'
	)
	mode '0644'
end

#
# TEST SITE
#
mysql_database "soliscrm_behat" do
	connection mysql_connection_info
end

mysql_database_user "behat_user" do
	connection mysql_connection_info
	password   "soliscrm behat user"
	database_name "soliscrm_behat"
	privileges [:all]
	action :grant
end

site_path = "#{www_path}/behat"
directory site_path do
	owner "vagrant"
	group "vagrant"
	mode "0750"
end

bash 'copy_wordpress_behat' do
	user "root"
	code "cp -Ru #{wordpress_src}/wordpress/* #{site_path}/"
end

# Create config file
vars = {
	:auth_key => secure_password(128),
	:secure_auth_key => secure_password(128),
	:logged_in_key => secure_password(128),
	:nonce_key => secure_password(128),
	:auth_salt => secure_password(128),
	:secure_auth_salt => secure_password(128),
	:logged_in_salt => secure_password(128),
	:nonce_salt => secure_password(128),
	:db_name => 'soliscrm_behat',
	:db_user => 'behat_user',
	:db_pass => 'soliscrm behat user',
	:db_host => 'localhost',
	:prefix => 'wp_',
	:wp_content_dir => wp_content_path,
	:wp_content_url => 'http://localhost:1988/wp-content/'
}
template "#{site_path}/wp-config.php" do
	source 'wp-config.php.erb'
	owner 'vagrant'
	group 'vagrant'
	variables vars
	mode  '0644'
	not_if { File.exists? "#{site_path}/wp-config.php" }
end

template "#{site_path}/install.php" do
	source 'install.php.erb'
	owner 'vagrant'
	group 'vagrant'
	variables(
		:home_url => 'http://localhost:1988/behat/'
	)
	mode '0644'
end

bash 'chown_stuff' do
	user "root"
	code <<-EOH
	chown -R vagrant:vagrant #{www_path}
	chown -R vagrant:vagrant #{wp_content_path}
	rm -Rf #{www_path}/dev/wp-content
	rm -Rf #{www_path}/behat/wp-content
	EOH
end

service "php-fpm" do
	action :restart
end

service "nginx" do
	action :reload
end

http_request "install_wordpress" do
	action :get
	url "http://localhost:1988/dev/install.php"
end

http_request "install_wordpress" do
	action :get
	url "http://localhost:1988/behat/install.php"
end

file "#{www_path}/dev/install.php" do
	action :delete
end

file "#{www_path}/behat/install.php" do
	action :delete
end

git tests_path do
	repository 'https://bitbucket.org/solislab/soliscrm-test.git'
	reference "master"
	action :sync
	user 'vagrant'
	group 'vagrant'
end

git "#{wp_content_path}/plugins/soliscrm" do
	repository 'https://bitbucket.org/solislab/soliscrm.git'
	reference 'master'
	action :sync
	user 'vagrant'
	group 'vagrant'
end

template "#{tests_path}/behat-custom.yml" do
	source 'behat-custom.yml.erb'
	owner 'vagrant'
	group 'vagrant'
	variables(
		:username => 'admin',
		:password => 'admin',
		:base_url => 'http://localhost:1988/behat'
	)
end

remote_file "#{Chef::Config[:file_cache_path]}/composer_installer.php" do
	source "https://getcomposer.org/installer"
	not_if { File.exists?('/usr/local/bin/composer') }
end

bash "install_composer" do
	user 'root'
	group 'root'
	code "php #{Chef::Config[:file_cache_path]}/composer_installer.php --install-dir=\"/usr/local/bin/\" --filename=\"composer\""
	not_if { File.exists?('/usr/local/bin/composer') }
end

bash "install_pear" do
	user 'root'
	group 'root'
	code <<-EOH
	pear config-set auto_discover 1
	pear install pear.phpunit.de/PHPUnit
	EOH
	not_if 'pear list phpunit/PHPUnit'
end

bash "install_dependencies" do
	user 'vagrant'
	group 'vagrant'
	code <<-EOH
	cd #{tests_path}
	composer install --prefer-dist
	EOH
	not_if { File.exists?("#{tests_path}/bin/behat") }
end

git "#{tests_path}/vendor/wordpress-unit-test" do
	repository 'https://bitbucket.org/solislab/wordpress-unit-test.git'
	reference "master"
	action :sync
	user 'vagrant'
	group 'vagrant'
end

template "#{tests_path}/vendor/wordpress-unit-test/wp-tests-config.php" do
	source 'wp-tests-config.php.erb'
	owner 'vagrant'
	group 'vagrant'
	variables(
		:db_name => 'soliscrm_unit',
		:db_user => 'unit_user',
		:db_pass => 'soliscrm unit user',
	)
end

template "#{tests_path}/phpunit-config.php" do
	source 'phpunit-config.php.erb'
	owner 'vagrant'
	group 'vagrant'
	variables(
		:plugin_path => "#{wp_content_path}/plugins/soliscrm/soliscrm.php"
	)
end

template "/home/vagrant/.gitconfig" do
	source 'gitconfig.erb'
	owner 'vagrant'
	group 'vagrant'
	not_if { File.exists?('/home/vagrant/.gitconfig') }
end

template "/home/vagrant/.solis-alias" do
	source 'alias.sh.erb'
	owner 'vagrant'
	group 'vagrant'
	mode '0744'
end

bash "source_alias" do
	user 'vagrant'
	group 'vagrant'
	not_if 'grep "source /home/vagrant/.solis-alias" /home/vagrant/.profile'
	code 'echo "source /home/vagrant/.solis-alias" >> /home/vagrant/.profile'
end