# Nginx
default['nginx']['install_method'] = 'package'
default['nginx']['default_site_enabled'] = false

# MySQL
default['mysql']['remove_anonymous_users'] = true
default['mysql']['remove_test_database'] = true
default['mysql']['server_debian_password'] = 'iloverandompasswordbutthiswilldo'
default['mysql']['server_root_password'] = 'iloverandompasswordbutthiswilldo'
default['mysql']['server_repl_password'] = 'iloverandompasswordbutthiswilldo'